let imgCounter = 1;
let dexitData ;
let pokemonData;
const pkmContent = document.getElementById("pokemon-content");

async function populate() {
    const response = await fetch('./assets/data/pkmn.json');
    pokemonData = await response.json();
    Object.keys(pokemonData).sort().forEach(function (index) {

        let colElement = document.createElement("div");
        colElement.className = "col";
        colElement.dataset.pokeId = pokemonData[index].idx;

        let rowElement = document.createElement("div");
        rowElement.className = "row";

        let newElement = document.createElement("div");
        newElement.className = "col pokeCol";

        let headElement = document.createElement("div");
        headElement.innerHTML = pokemonData[index].name.eng;

        //let imgElement = document.createElement("img");
        let imgElement = new Image();
        imgElement.onload = function() {
            imgCounter++;
            if(imgCounter === 809) {
                console.log("loaded");
                revealSnap();
            }
        };
        imgElement.src = './assets/icons/'+ index + ".png";

        newElement.append(imgElement);
        newElement.append(headElement);
        rowElement.append(newElement);
        colElement.append(rowElement);
        pkmContent.append(colElement);

    });

}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element);
    node.classList.add('animated', animationName);

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName);
        node.removeEventListener('animationend', handleAnimationEnd);

        if (typeof callback === 'function') callback();
    }

    node.addEventListener('animationend', handleAnimationEnd);
}

async function populateDexitInfo() {
    const response = await fetch('./assets/data/dexit.json');
    dexitData = await response.json();
}


function snapped() {
    animateCSS('.container-fluid', 'shake');
    let dexitKeys = Object.keys(dexitData).sort();
    let pokemonKeys = Object.keys(pokemonData).sort();
    let intersection = pokemonKeys.filter(x => !dexitKeys.includes(x));
    for(let loopCounter = 1; loopCounter < imgCounter; loopCounter++) {
        if(parseInt(intersection[loopCounter-1])) {
            let parsedIndex = parseInt(intersection[loopCounter-1]);
            let dataAttr = `div[data-poke-id="${parsedIndex}"]`;
            animateCSS(dataAttr, 'fadeOut', function () {
                document.querySelector(dataAttr).style.display = "none";
            });
        }
    }
}

function revealSnap() {
    document.getElementById("snapButton").style.display = "initial";
}

populate();
populateDexitInfo();
